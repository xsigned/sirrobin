<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 pr-0 pl-0" >
					<div class="front-images-container">
						<img src="/wp-content/themes/understrap/img/test.svg" class="three-climates-land-img" />
						<img src="/wp-content/themes/understrap/img/ventilator.svg" class="front-image-ventilator" />
						<img src="/wp-content/themes/understrap/img/air_conditioner.svg" class="front-image-air" />
						<div class="glow"></div>
						<div class="s0 cold"></div>
						<div class="s1 cold"></div>
						<div class="s2 cold"></div>
						<div class="s3 cold"></div>
						<div class="s4 cold"></div>
						<div class="s5 cold"></div>
						<div class="s6 cold"></div>
						<div class="s7 cold"></div>
						<div class="s8 cold"></div>
						<div class="s9 cold"></div>
						<div class="light"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 pr-0 pl-0">
					<a href="<?php pll_e('front-page-0-0-link'); ?>">
						<div class="card bg-primary rounded-0 bg-grey front-page-option"">
							<div class="card-body">
								<h4 class="card-title text-white"><?php pll_e('front-page-0-0'); ?></h4>
								<h6 class="card-subtitle mb-2 text-grey"><?php pll_e('front-page-0-0-sub'); ?></h6>
								<!--<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>-->
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-6 pr-0 pl-0 ">
					<a href="<?php pll_e('front-page-0-1-link'); ?>">
						<div class="card bg-primary rounded-0 bg-grey front-page-option"">
							<div class="card-body">
								<h4 class="card-title text-white"><?php pll_e('front-page-0-1'); ?></h4>
								<h6 class="card-subtitle mb-2 text-grey"><?php pll_e('front-page-0-1-sub'); ?></h6>
								<!--<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>-->
							</div>
						</div>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 pr-0 pl-0">
					<a href="<?php pll_e('front-page-1-0-link'); ?>">
						<div class="card bg-primary rounded-0 bg-grey front-page-option">
							<div class="card-body">
								<h4 class="card-title text-white"><?php pll_e('front-page-1-0'); ?></h4>
								<h6 class="card-subtitle mb-2 text-grey"><?php pll_e('front-page-1-0-sub'); ?></h6>
								<!--<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>-->
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-6 pr-0 pl-0 front-page-option">
					<a href="<?php pll_e('front-page-1-1-link'); ?>">
						<div class="card bg-primary rounded-0 bg-grey">
							<div class="card-body">
								<h4 class="card-title text-white"><?php pll_e('front-page-1-1'); ?> </h4>
								<h6 class="card-subtitle mb-2 text-grey"><?php pll_e('front-page-1-1-sub'); ?></h6>
								<!--<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>-->
							</div>
						</div>
					</a>
				</div>				
			</div>
			<div class="row">
				<div class="col-lg-12 pr-0 pl-0" >
					<div class="front-images-container">
						<img src="/wp-content/themes/understrap/img/background_part2.svg" class="three-climates-land-img" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 pr-0 pl-0" >
					<?php get_footer(); ?>
				</div>
			</div>
		</div>

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
		<div class="row">
		
			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">


			</main><!-- #main -->
		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #page-wrapper -->


